# Outlet Hering Store


##### General Info

`Doc Version v0.0.1`

`Last Updated: 2020/07/17`

`Project Based on VTEX-IO Environment`

`Clockify Task:`

[FIGMA LINK]()

---

### Project Guidelines


| **REQUIRED** |
|------------| 
|Mobile First|
|[Semantic Versioning](https://semver.org/)|
|[Gitflow Practices](https://www.udacity.com/course/version-control-with-git--ud123)|
|[Prettier Extension](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)|
|Definir builder checkout|
|[Gitlab Board](https://docs.gitlab.com/ee/user/project/issue_board.html)|


|**SUGGESTED**|
|----|





## Emulating a Workflow


#### Gitlab and issue distribution

First of all, pick an issue on the [board](https://gitlab.com/groups/acct.global/acct.spartanos/-/boards) at the **to-do** column.

Make sure to move the cards to the appropriate column for each new step and to mark the issue with the proper [labels](https://gitlab.com/groups/acct.global/acct.spartanos/-/labels).

When a task is finished, please tag the project manager on the comments of the original issue and provide a link for the final validation.

Make sure to reference the issue at the Merge Request (see below);

###### Merge Request Template

```
Ref #123        OR        Ref [#123](Link for another Repo)

[Mobile/Desktop Specification]

Short Description of the issue

Workspace: https://link-to-my-vtexio-workspace-for-validation.com

```
 
 ###### Merge Request EXAMPLE
 
```

Ref #375  

[ Mobile ]

- Hamburguer menu adjustments;

- Login font fixes;

Workspace: hamburguer-menu--storename.myvtex.com
```

###### Deployment and Versioning templates

**Branch:** `Release/0.0.0`

**Tagging:** `v0.0.0`

----
### Project Related Components & Pixel Apps


| Component     | Creation Date | Version|
| ------------- |:-------------:| ------:|
| [Name](Link)  | YYYY/MM/DD    | v0.0.0 |
| [Name](Link)  | YYYY/MM/DD    | v0.0.0 |
| [Name](Link)  | YYYY/MM/DD    | v0.0.0 |

---
#### Useful Docs

[VTEX-IO Getting Started](https://vtex.io/docs/getting-started/what-is-vtex-io/1/)

[VTEX APPS Github](https://github.com/vtex-apps)

[VTEX Developers Reference](https://developers.vtex.com/)

[VTEX Styleguide](https://styleguide.vtex.com/)

---

### Extra

Need anything else? `@spartanos` at [#squad-spartanos](https://acctglobal.slack.com/archives/C0123ALV6LQ) channel =)